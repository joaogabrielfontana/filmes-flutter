import 'package:filmes_tokenlab/Models/MovieModel.dart';
import 'package:flutter/material.dart';
import '../Widgets/Movie.dart';
import '../Models/MovieModel.dart';

class MovieGrid extends StatelessWidget {
    final List<MovieModel> movies;

    MovieGrid(this.movies);
    @override

    Widget build(BuildContext context) {
        var size = MediaQuery.of(context).size;

        final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
        final double itemWidth = size.width / 2;

        return Container(
          margin: new EdgeInsets.all(10),
          child: GridView.builder(
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: itemWidth/(itemHeight + 30),
              ),
              itemCount: movies.length,
              itemBuilder: (context, index) {
                  return Movie(movies[index]);
              }),
        );
  }


}