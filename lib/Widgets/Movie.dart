import 'package:flutter/material.dart';
import '../Models/MovieModel.dart';

class Movie extends StatelessWidget {
    final MovieModel movie;

   Movie(this.movie);

    @override
    Widget build(BuildContext context) {

       return LayoutBuilder(builder: (ctx, constraints) {
           return Container(
               margin: new EdgeInsets.all(5),
               decoration: BoxDecoration(color: Colors.lightBlue, borderRadius: new BorderRadius.all(Radius.circular(4))),
             child: Column(
                 children: <Widget>[
                 Container(
                     margin: new EdgeInsets.only(top: 10),
                     height: constraints.maxHeight * 0.6,
                     child: Image.asset(movie.posterUrl)
                     ,),
                 SizedBox(height: 10,),
                 Text(movie.movieName,),
                 SizedBox(height: 10,),
                 Text(movie.movieReleaseYear),
                 SizedBox(height: 10,),
                 Text(movie.movieGenre),
             ],),
           );
       });
  }
}