import 'package:flutter/cupertino.dart';

class MovieModel {
    final movieName;
    final movieReleaseYear;
    final movieGenre;
    final posterUrl;

    MovieModel({@required this.movieName, @required this.movieReleaseYear, @required this.movieGenre, @required this.posterUrl});

}