import 'package:flutter/material.dart';
import './Models/MovieModel.dart';
import './Widgets/MovieGrid.dart';


class FilmesHome extends StatefulWidget {
  @override
  _FilmesHomeState createState() => _FilmesHomeState();
}

class _FilmesHomeState extends State<FilmesHome> {


  final List<MovieModel> movies = [ MovieModel(movieName: "Cuphead", movieGenre: "Action", movieReleaseYear: "2017", posterUrl: "assets/cuphead.jpg"),
      MovieModel(movieName: "Cuphead", movieGenre: "Action", movieReleaseYear: "2017", posterUrl: "assets/cuphead.jpg"),
      MovieModel(movieName: "Cuphead", movieGenre: "Action", movieReleaseYear: "2017", posterUrl: "assets/cuphead.jpg"),
      MovieModel(movieName: "Cuphead", movieGenre: "Action", movieReleaseYear: "2017", posterUrl: "assets/cuphead.jpg"),
      MovieModel(movieName: "Cuphead", movieGenre: "Action", movieReleaseYear: "2017", posterUrl: "assets/cuphead.jpg"),
      MovieModel(movieName: "Cuphead", movieGenre: "Action", movieReleaseYear: "2017", posterUrl: "assets/cuphead.jpg")
  ];

  @override
  Widget build(BuildContext context) {
     final appBar =  AppBar(
         title: Text("Meu primeiro App Flutter"),
     );

    return Scaffold(
      appBar: appBar,
      body: Container(
          color: Colors.teal,
          height: (MediaQuery.of(context).size.height -
              appBar.preferredSize.height -
              MediaQuery.of(context).padding.top),
          child: MovieGrid(this.movies),
      ),
    );
  }
}
